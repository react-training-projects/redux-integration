/*
  reducers
*/
import { combineReducers } from 'redux';
import { SELECT_SONG } from '../actions';

const songsReducer = () => {
  return[
    {title: 'No Scrubs', duration: '4:05'},
    {title: 'Macarena', duration: '2:30'},
    {title: 'All Star', duration: '3:15'},
    {title: 'I Want It That Way', duration: '1:45'}
  ];
};

const selectedSongReducer = (song = null, action) => {
  if (action.type === SELECT_SONG) {
    song = action.payload;
  }

  return song;
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer
});
