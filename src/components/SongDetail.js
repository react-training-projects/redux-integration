import React from 'react';
import { connect } from 'react-redux';

const SongDetail = ({ song }) => {
  var content;

  if (!song) {
    content = (
      <div className="song-detail">
        <h2>Select a song!</h2>
      </div>
    );
  } else {
    content = (
      <div className="song-detail">
        <h1>Song Details</h1>
        <h3>Title - {song.title}</h3>
        <h4>Duration - {song.duration}</h4>
      </div>
    );
  }

  return content;
};

const mapStateToProps = (state) => {
  return { song: state.selectedSong };
}

export default connect(mapStateToProps)(SongDetail);
